//Problem: https://www.hackerrank.com/challenges/grid-challenge

#include<cstdio>
#include<iostream>
#include<vector>
#include<map>
#include<algorithm>

using namespace std;
 
int main(){
    int T; 
   cin>>T;
    while(T--){
        int N; 
        cin>>N;
        char A[N][N];
        
        for(int n = 0; n<N; n++)
        {
            cin>>A[n];
            sort(A[n], A[n]+N);
        }
        bool b = false;
        for(int i = 0; i<N; i++)
        {
            if(b) 
                break;
            for(int j = 0; j<N-1; j++)
            {
                if(A[j+1][i]<A[j][i])
                {
                    b = true;
                    break;
                }
            }
        }
        if(!b) 
            cout<<"YES";
        else 
            cout<<"NO";
    }
    return 0;
}
